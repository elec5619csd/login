package elec5619.edu.au.login.dao;

import elec5619.edu.au.login.beans.Customer;

public interface CustomerDao {
	
	public void saveCustomer(Customer customer);

	public Customer loginCustomer(Customer customer);

}
