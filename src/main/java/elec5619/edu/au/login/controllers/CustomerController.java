package elec5619.edu.au.login.controllers;

import java.util.Locale;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import elec5619.edu.au.login.beans.Customer;
import elec5619.edu.au.login.services.CustomerService;
import elec5619.edu.au.login.validation.CustomerValidation;

@Controller
@RequestMapping(value = "/customer")
public class CustomerController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		return "home";
	}
	
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "/register", method=RequestMethod.GET)
	public String showForm(ModelMap model){
		model.put("customerData", new Customer());
		return "register/register";
		
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String saveForm(ModelMap model, @ModelAttribute("customerData")@Valid Customer customer, BindingResult br, HttpSession session)
	{
		CustomerValidation customerValidation = new CustomerValidation();
		customerValidation.validate(customerValidation, br);
		if (br.hasErrors())
		{
			return "register/register";
		}
		else
		{
			customerService.saveCustomer(customer);
			session.setAttribute("customer", customer);
			return "redirect:success";
		}
		
	}
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLogin(ModelMap model, HttpSession session){
		
		if (session.getAttribute("customer") == null)
		{
			model.put("customerData", new Customer());
			return "login/login";
		}
		else
		{	
			return "redirect:success";
		}
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String doLogin(ModelMap model, @ModelAttribute("customerData") Customer customer, HttpSession session){
		
		if (customer.getC_email() != null && customer.getC_password() != null && session.getAttribute("customer") == null){
			customer = customerService.loginCustomer(customer);
			if (customer != null)
			{
				session.setAttribute("customer", customer);
				return "redirect:success";
			}
			else
			{
				model.put("failed", "Login Failed.");
				return "login/login";
			}
		}
		else
			return  "redirect:" + "success";
	}
	
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logOut(ModelMap model, HttpSession session){
		session.removeAttribute("customer");
		return "redirect:login";
	}

	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public String showSuccess(ModelMap model, HttpSession session){
		model.put("success", new Customer());
		return "success";
	}
	
	
	
}
