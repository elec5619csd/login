package elec5619.edu.au.login.services;

import elec5619.edu.au.login.beans.Customer;

public interface CustomerService {
	
	public void saveCustomer(Customer customer);

	public Customer loginCustomer(Customer customer);
}
