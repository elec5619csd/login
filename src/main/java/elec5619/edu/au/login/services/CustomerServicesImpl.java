package elec5619.edu.au.login.services;

import org.springframework.beans.factory.annotation.Autowired;

import elec5619.edu.au.login.beans.Customer;
import elec5619.edu.au.login.dao.CustomerDao;

public class CustomerServicesImpl implements CustomerService {

	@Autowired
	private CustomerDao customerDao;
	
	public void setCustomerDao(CustomerDao customerDao){
		this.customerDao = customerDao;
		
	}
	
	@Override
	public void saveCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
		customerDao.saveCustomer(customer);

	}

	@Override
	public Customer loginCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
		return customerDao.loginCustomer(customer);
	}

}
