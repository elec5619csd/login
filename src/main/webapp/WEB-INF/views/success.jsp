<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<html>
	<head>
		<title>Welcome</title>
	</head>
	<body>
		<h1>${msg}</h1>
		<a href="${pageContext.request.contextPath}/customer/logout"><b>logout</b></a>
		
		<br />
		<br />
		<div>
			Welcome ${customer.c_name}
		</div>
	</body>

</html>