<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<html>
	<head>
		<title>Register</title>
	</head>
	<body>
		<h1>Register Customer</h1>
		<table>
			<s:form commandName="customerData" 
			action="${pageContext.request.contextPath}/customer/login" metohd="post">
				<tr>
					<td>
						Customer email:
					</td>
					<td>
						<s:input path="c_email"/>
					</td>
				</tr>
				<tr>
					<td>
						Customer password:
					</td>
					<td>
						<s:input path="c_password"/>
					</td>
					
				</tr>
				<tr>
					<td><input type="submit" value="Login" /></td>
				</tr>
			
			</s:form>
			<tr><td><a href="${pageContext.request.contextPath}/customer/register">Register</a></td></tr>
			<tr><td><a href="${pageContext.request.contextPath}/">Home</a></td></tr>
			
			
			
		</table>
		
		<p style="color:red;">${failed}</p>
		
		
		
		
	</body>


</html>