<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<html>
	<head>
		<title>Register</title>
	</head>
	<body>
		<h1>Register Customer</h1>
		<table>
			<s:form commandName="customerData" 
			action="${pageContext.request.contextPath}/customer/register" metohd="post">
				<tr>
					<td>
						Customer Name:
					</td>
					<td>
						<s:input path="c_name"/>
					</td>
					<td>
						<s:errors path="c_name" cssStyle = "color:red;" />
					</td>
					
				</tr>
				<tr>
					<td>
						Customer mobile:
					</td>
					<td>
						<s:input path="c_mobile"/>
					</td>
					<td>
						<s:errors path="c_mobile" cssStyle = "color:red;" />
					</td>
					
				</tr>
				<tr>
					<td>
						Customer Gender:
					</td>
					<td>
						<s:radiobutton path="c_gender" value="Male" name="gender"/> Male
						<s:radiobutton path="c_gender" value="Female" name="gender"/> Female
					</td>
					<td>
						<s:errors path="c_gender" cssStyle = "color:red;" />
					</td>
					
				</tr>
				<tr>
					<td>
						Customer email::
					</td>
					<td>
						<s:input path="c_email"/>
					</td>
					<td>
						<s:errors path="c_email" cssStyle = "color:red;" />
					</td>
					
				</tr>
				<tr>
					<td>
						Customer password:
					</td>
					<td>
						<s:input path="c_password"/>
					</td>
					<td>
						<s:errors path="c_password" cssStyle = "color:red;" />
					</td>
					
				</tr>
				<tr>
					<td><input type="submit" value="Register" /></td>
				</tr>
			
			</s:form>
		</table>
		
		
		
		
		
		
	</body>


</html>